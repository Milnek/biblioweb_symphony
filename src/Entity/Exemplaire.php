<?php

namespace App\Entity;

use App\Repository\ExemplaireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExemplaireRepository::class)]
class Exemplaire
{
    #[ORM\Id]
    //#[ORM\GeneratedValue]
    //#[ORM\Column]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $numero = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity:"App\Entity\Livre", inversedBy: "livres")]
    #[ORM\JoinColumn(name:"idlivre", referencedColumnName:"id")]
    private ?Livre $livre;

    #[ORM\ManyToOne(targetEntity:"App\Entity\Lecteur", inversedBy: "lecteurs")]
    #[ORM\JoinColumn(name:"emprunteur", referencedColumnName:"id")]
    private ?Lecteur $lecteur;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateretour = null;

        public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function getDateretour(): ?\DateTimeInterface
    {
        return $this->dateretour;
    }

    public function setDateretour(?\DateTimeInterface $dateretour): self
    {
        $this->dateretour = $dateretour;

        return $this;
    }

    public function retard(): ?string
    {
        return 'retard';
        // TODO : fonction calculant le retard
        // todo : https://moodle.lyc-coliniere-44.ac-nantes.fr/mod/assign/view.php?id=13991
    }


    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }
}
