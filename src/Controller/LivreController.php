<?php

namespace App\Controller;

use App\Entity\Livre;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LivreController extends AbstractController {
    public function livres(ManagerRegistry $doctrine): Response {
        $listelivres = $doctrine->getRepository(Livre::class)->findAll();
        return $this->render('livres\livres.html.twig', ['titre' => 'Liste des livres', 'livres' => $listelivres]);
    }

    public function details(ManagerRegistry $doctrine, int $id): Response {
        $livre = $doctrine->getRepository(Livre::class)->find($id);
        if (! $livre) {
            throw $this->createNotFoundException('Aucun livre pour l\'id '. $id);
        } else {
            return $this->render('livres\details.html.twig', ['titre' => 'Details du livre', 'livre' => $livre]);
        }
    }

    public function detailsLivreGenres(ManagerRegistry $doctrine, $id) {
        $livre = $doctrine->getRepository(Livre::class)->find($id);
        if (!$livre) {
            throw $this->createNotFoundException('Aucun livre pour l\'id ' . $id);
        } else {
            return $this->render('livres\detailsLivreGenres.html.twig', ['titre' => 'Genres du livre', 'livre' => $livre]);
        }
    }
}