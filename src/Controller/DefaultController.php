<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(): Response {
        return $this->render('default/index.html.twig', ['titre' => "Bienvenue sur la page d'accueil"]);
    }
    public function recherche(): Response {
        return $this->render('default/recherche.html.twig', ['titre' => "Rechercher sur Biblioweb"]);
    }
    public function connexion(): Response {
        return $this->render('default/connexion.html.twig', ['titre' => "Veuillez vous connecter"]);
    }
    public function infopratiques(): Response {
        return $this->render('default/infopratiques.html.twig', ['titre' => "Informations Pratiques"]);
    }
}