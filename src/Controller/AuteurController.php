<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\Livre;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AuteurController extends AbstractController {
    public function auteurs(ManagerRegistry $doctrine): Response {
        $listeauteurs = $doctrine->getRepository(Auteur::class)->findAll();
        return $this->render('auteurs\auteurs.html.twig', ['titre' => 'Liste des auteurs', 'auteurs' => $listeauteurs]);
    }

    public function details(ManagerRegistry $doctrine, $id) {
        $auteur = $doctrine->getRepository(Auteur::class)->find($id);
        if (! $auteur) {
            throw $this->createNotFoundException('Aucun auteur pour l\'id ' . $id);
        } else {
            return $this->render('auteurs\details.html.twig', ['titre' => "Details de l'auteur", 'auteur' => $auteur]);
        }
    }

    public function detailsAuteurLivres(ManagerRegistry $doctrine, $id): Response {
        $auteur = $doctrine->getRepository(Auteur::class)->find($id);
        if (! $auteur) {
            throw $this->createNotFoundException('Aucun auteur pour l\'id ' . $id);
        } else {
            $livres = $doctrine->getRepository(Livre::class)->findByAuteur($auteur);
            return $this->render('auteurs\detailsAuteurLivres.html.twig', ['titre' => "Liste des livres de l'auteur", 'auteur' => $auteur, 'livres' => $livres]);
        }
    }
}





