<?php

namespace App\Controller;

use App\Entity\Exemplaire;
use App\Entity\Lecteur;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LecteurController extends AbstractController
{
    public function lecteurs(ManagerRegistry $doctrine): Response {
        $listelecteurs = $doctrine->getRepository(Lecteur::class)->findAll();
        return $this->render('lecteurs\lecteurs.html.twig', ['titre' => 'Liste des lecteurs', 'lecteurs' => $listelecteurs]);
    }

    public function details(ManagerRegistry $doctrine, $id) {
        $lecteur = $doctrine->getRepository(Lecteur::class)->find($id);
        if (! $lecteur) {
            throw $this->createNotFoundException('Aucun lecteur pour l\'id ' . $id);
        } else {
            return $this->render('lecteurs\details.html.twig', ['titre' => "Details du lecteur", 'lecteur' => $lecteur]);
        }
    }
}