<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class GenreController extends AbstractController
{
    #[Route('/genres.html.twig', name: 'app_genre')]
    public function genres(ManagerRegistry $doctrine): Response {
        $listeGenres = $doctrine->getRepository(Genre::class)->findAll();
        return $this->render('genres\genres.html.twig', ['titre' => 'Liste des genres', 'genres' => $listeGenres]);
    }


    public function detailsGenreLivres(ManagerRegistry $doctrine, int $id): Response {
        $genre = $doctrine->getRepository(Genre::class)->find($id);
        if (! $genre) {
            throw $this->createNotFoundException('Aucun genre pour l\'id ' . $id);
        } else {
            return $this->render('genres\detailsGenreLivres.html.twig', ['titre' => 'Liste des livres du genre', 'genre' => $genre]);
        }
    }
}
