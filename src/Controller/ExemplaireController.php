<?php

namespace App\Controller;

use App\Entity\Exemplaire;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExemplaireController extends AbstractController
{
    public function exemplaires(ManagerRegistry $doctrine): Response {
        $listeExemplaires = $doctrine->getRepository(Exemplaire::class)->findAll();
        return $this->render('exemplaires/exemplaires.html.twig', ['titre' => 'Liste des exemplaires', 'exemplaires' => $listeExemplaires]);
    }

    public function toFrench(string $entree): string { // TODO : à faire fonctionner (utilisé dans les details des auteurs et dans exemplaires)
        $jour = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
        $day = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $mois = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
        return 'MOA';
    }
}
