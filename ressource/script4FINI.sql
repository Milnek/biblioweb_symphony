--Créer une procédure stockée qui calcule pour un emprunt en cours pour un lecteur, le nombre de jours restant avant d être en retard (si le lecteur est en retard, cela renvoie un résultat négatif correspondant au nombre de jours de retard, si le lecteur n est pas en retard, cela renvoie un résultat positif correspondant au nombre de jours restant).
drop function calculRetard;
create or replace function calculRetard(integer, integer) returns integer as
--                                $1 = idLivre | $2 = emprunteur
$$
declare
dateDeRetour date;
begin
select into dateDeRetour dateretour from exemplaire where idLivre = $1 and emprunteur = $2;
return dateDeRetour - date(now());
end;
$$
language 'plpgsql';

select calculRetard(2, 1);-- expect 3, le 07/12/2022 / expect -2, le 12/12/2022
select calculRetard(3, 1);-- expect -362, le 07/12/2022
select calculRetard(1, 1);-- expect 15, le 07/12/2022


