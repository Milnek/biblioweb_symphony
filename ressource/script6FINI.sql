--Créer une procédure stockée renvoyant les exemplaires disponibles pour un livre donné
create or replace function exemplairesDispoByIdLivre(integer) returns table(numerodelexemplaire integer) as
$$
DECLARE
BEGIN
return query select numero from exemplaire where idLivre=$1 and dateretour is NULL;
END;
$$
LANGUAGE 'plpgsql';

select exemplairesDispoByIdLivre(1) --expect [vide];
select exemplairesDispoByIdLivre(6) --expect 2 et 3;
select exemplairesDispoByIdLivre(4) --expect 2;

