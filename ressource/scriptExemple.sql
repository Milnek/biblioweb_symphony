--https://www.postgresqltutorial.com/postgresql-plpgsql/plpgsql-if-else-statements/

--EXEMPLE : procédure stockée renvoyant  le nombre d'emprunts en cours
CREATE FUNCTION nbEmpruntEnCours()  RETURNS integer AS
    $$
DECLARE
nbEmprunt integer;
BEGIN
select into nbEmprunt count(numero) from exemplaire where dateretour is not null;
RETURN nbEmprunt;
END;
$$
LANGUAGE 'plpgsql';

select nbEmpruntEnCours();