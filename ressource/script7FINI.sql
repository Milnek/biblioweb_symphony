--Créer une procédure qui renvoie les titres des livres d un auteur et le nombre d exemplaires disponibles par titre.

drop function titresByAuteurAndNbExemplairesDispoByTitre;

create or replace function titresByAuteurAndNbExemplairesDispoByTitre(integer) returns table(titrelivre varchar, nBExempDispo bigint) as
$$
declare
begin
return query select l.titre, COUNT(numero) from livre l left join exemplaire e on l.id = e.idlivre where idAuteur = $1 and dateretour is null group by l.titre order by l.titre;
end;
$$
language 'plpgsql';

-- PROBLEME DE JOIN

select titresByAuteurAndNbExemplairesDispoByTitre(2);-- expect 4 rows GOT 3 rows / expect ("La horde du contrevent",1), ("La zone du dehors",2), ("Tara Duncan : Le livre  interdit",0), ("Les furtifs",0) GOT ("La horde du contrevent",1), ("La zone du dehors",2), ("Tara Duncan : Le livre  interdit",0)
select titresByAuteurAndNbExemplairesDispoByTitre(4);-- expect ("Les Mensonges de Locke Lamora",2)
select titresByAuteurAndNbExemplairesDispoByTitre(3);

