--Créer une procédure permettant de créer un nouvel emprunt sachant que la durée de l emprunt est fixée à 15 jours (elle devra gérer les cas d erreurs : l exemplaires n existe pas, le lecteur n existe pas, l exemplaires est déjà emprunté, le lecteur a déjà emprunté ce livre). La procédure renvoie un code 0 OK ou code d erreur (1,2 ou 3 selon l erreur).
create or replace function createNewEmprunt(integer, integer, integer) returns integer as
--                                        $1 = idLivre | $2 = numero | $3 = emprunteur
$$
declare
code integer;
	test integer;
	dateDeRetour date;
begin
	code := 0;--0 : OK par défaut

	perform * from exemplaire where numero = $2 and idLivre = $1;
	if not found then
		code := 1;--1 : l exemplaires n existe pas
end if;

	perform * from lecteur where id = $3;
	if not found and code=0 then
		code := 2;--2 : le lecteur n existe pas
end if;

	perform * from exemplaire where idlivre = $1 and numero = $2 and emprunteur != $3;
	if found and code=0 then
		code := 3;--3 : l exemplaires est déjà emprunté
end if;

	perform * from exemplaire where emprunteur = $3 and idLivre = $1;
	if found and code=0 then
		code := 4;--4 : le lecteur a déjà emprunté ce livre
end if;

	if code = 0 then
select into dateDeRetour date(now() + interval '15 days');
update exemplaire set dateretour = dateDeRetour, emprunteur = $3 where numero = $2 and idlivre = $1;
end if;

return code;
end;
$$
language 'plpgsql';
select createNewEmprunt(1, 1, 2); --expect 3
select createNewEmprunt(4, 2, 3); --expect 0, usage unique utilisé, fonctionnel
select createNewEmprunt(1, 1, 1); --expect 4
select createNewEmprunt(1, 1, 25); --expect 2
select createNewEmprunt(1, 25, 25); --expect 1
select createNewEmprunt(1, 25, 1); --expect 1


