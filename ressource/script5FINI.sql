--Créer une procédure permettant de renvoyer les emprunts d un lecteurs identifié par son numéro.
create or replace function empruntsByidEmprunteur(integer) returns table(datederetour date, iddulivre integer, numerodelexemplaire integer) as
$$
declare
begin
return query select dateretour, idLivre, numero from exemplaire where emprunteur = $1;
end;
$$
language 'plpgsql';
select empruntsByidEmprunteur(2); --expect[vide]
select empruntsByidEmprunteur(4); --expect (2021-11-30,3,2), (2021-11-30,4,3), (2022-12-15,1,2)
select empruntsByidEmprunteur(1); --expect (2021-12-10,2,1), (2021-12-10,3,1), (2021-12-10,4,1), (2021-12-10,5,1), (2021-12-10,6,1), (2022-12-15,1,1)
