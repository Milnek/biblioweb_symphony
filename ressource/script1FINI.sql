--Créer une procédure stockée renvoyant le nombre d exemplaires disponibles pour un livre donné
create or REPLACE FUNCTION nbExemplairesByIdLivres(integer)  RETURNS integer AS
$$
    DECLARE
nbExemplaires integer;
BEGIN
select into nbExemplaires count(numero) from exemplaire where idlivre = $1;
RETURN nbExemplaires;
END;
$$
LANGUAGE 'plpgsql';
select nbExemplairesByIdLivres(1); --expect 2
select nbExemplairesByIdLivres(7); --expect 0
select nbExemplairesByIdLivres(6); --expect 3