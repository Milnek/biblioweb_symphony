--Créer une procédure stockée renvoyant le nombre d emprunts pour un lecteurs donné
create or replace function nbEmpruntsByIdLecteur(integer) returns integer as
$$
	declare
nbEmprunts integer;
begin
select into nbEmprunts count(numero) from exemplaire where emprunteur = $1;
return nbemprunts;
end;
$$
language 'plpgsql';

select nbEmpruntsByIdLecteur(3); --expect 0
select nbEmpruntsByIdLecteur(1); --expect 6